﻿// Copyright (c) 2017 NG Branch Technology GmbH
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without modification,
// are permitted provided that the following conditions are met:
//
// Redistributions of source code must retain the above copyright notice, this list
// of conditions and the following disclaimer. Redistributions in binary form must
// reproduce the above copyright notice, this list of conditions and the following
// disclaimer in the documentation and/or other materials provided with the distribution.
// 
// Neither the name regdiff nor the names of its contributors may be used to endorse
// or promote products derived from this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
// ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
// WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
// IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
// INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
// NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
// PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
// WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace kitchensync
{
    public class RestoreResult
    {
        public readonly FileInfo FI;
        public readonly string Checksum;
        public readonly bool Success;
        public readonly string ReasonForFailure;

        public RestoreResult(FileInfo fi, Configuration.MachineSpecificConfiguration msc)
        {
            FI = fi;
            try
            {
                Checksum = ReadXmlFile();
                Success = false;
                if (!string.IsNullOrEmpty(Checksum))
                {
                    foreach (var ra in msc.RemoteArchives)
                    {
                        try
                        {
                            ra.Restore(FI, Checksum);
                            Success = true;
                            break;
                        }
                        catch (Exception exCaught)
                        {
                            ReasonForFailure = exCaught.ToString();
                        }
                    }
                    if (Success)
                    {
                        FI = new FileInfo(fi.FullName);
                    }
                }
            }
            catch (Exception e)
            {
                ReasonForFailure = e.ToString();
                Success = false;
            }
        }

        private string ReadXmlFile()
        {
            try
            {
                string text = File.ReadAllText(FI.FullName);
                if (!text.StartsWith("<?xml version=\"1.0\" encoding=\"utf-8\"?><kitchensync sha256="))
                    return null;

                using (XmlTextReader xmlReader = new XmlTextReader(FI.FullName))
                {
                    while (xmlReader.Read())
                    {
                        if (xmlReader.NodeType == XmlNodeType.Element)
                        {
                            if (xmlReader.Name == "kitchensync")
                            {
                                return xmlReader.GetAttribute("sha256");
                            }
                        }
                    }
                }

            }
            catch (Exception)
            {
            }
            return null;
        }

        public override string ToString()
        {
            if (Success)
            {
                return string.Format("{0} - {1,30} - OK [{2} bytes]",
                    Checksum,
                    FI.Name,
                    FI.Length);
            }
            else
            {
                return string.Format("{0} - {1,30} - KO [{2}]",
                    Checksum,
                    FI.Name,
                    ReasonForFailure);
            }

        }
    }
}
