﻿// Copyright (c) 2017 NG Branch Technology GmbH
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without modification,
// are permitted provided that the following conditions are met:
//
// Redistributions of source code must retain the above copyright notice, this list
// of conditions and the following disclaimer. Redistributions in binary form must
// reproduce the above copyright notice, this list of conditions and the following
// disclaimer in the documentation and/or other materials provided with the distribution.
// 
// Neither the name regdiff nor the names of its contributors may be used to endorse
// or promote products derived from this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
// ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
// WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
// IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
// INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
// NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
// PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
// WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Diagnostics;
using System.Security.Cryptography;
using System.Xml;

namespace kitchensync
{

    class Program
    {
        private readonly Configuration Configuration;

        public Program(string[] args)
        {
            Configuration = new Configuration(args);
        }

        private static TOutput[] ParallelMap<TInput, TOutput>(TInput[] source, Func<TInput, TOutput> mapFunction)
        {
            return source.AsParallel().AsOrdered().Select(x => mapFunction(x)).ToArray();
        }

        private bool Run()
        {
            Console.WriteLine("*** KITCHENSYNC {0} - Copyright (C) 2017 by NG Branch Technology GmbH ***", AppVersion.Get());
            Console.WriteLine();
            if (!Configuration.HasBeenRead)
            {
                Console.WriteLine("ERROR: kitchensync.xml configuration file missing or invalid.", Environment.MachineName);
                return false;
            }
            if (!Configuration.KnowsThisMachine())
            {
                Console.WriteLine("ERROR: this machine ({0}) is not known in the kitchensync.xml configuration file.", Environment.MachineName);
                return false;
            }
            if (!Configuration.RunBackup && !Configuration.RunRestore)
            {
                Console.WriteLine("ERROR: neither /BACKUP nor /RESTORE specified, don't know what to do...");
                return false;
            }
            if (Configuration.RunBackup)
            {
                Backup();
            }
            if(Configuration.RunRestore)
            {
                Restore();
            }
            return true;
        }

        private bool SelectRestoreFiles(FileInfo fi)
        { 
            if (fi.FullName.Contains("\\.git\\"))
                return false;

            if (fi.Length < 256)
                return true;

            return false;
        }

        private void Restore()
        {
            var msc = Configuration.CurrentConfig;

            var watch = System.Diagnostics.Stopwatch.StartNew();
            List<RestoreResult> results;
            if (msc.SupportsParallelExecution)
            {
                results = new List<RestoreResult>(new DirectoryInfo(".").EnumerateDirectories()
                        .AsParallel()
                        .SelectMany(di => di.EnumerateFiles("*.*", SearchOption.AllDirectories)
                                            .Where(SelectRestoreFiles))
                        .AsParallel()
                        .AsOrdered().Select(x => new RestoreResult(x, msc))
                        .ToArray());
            }
            else
            {
                results = new List<RestoreResult>();
                foreach (var di in new DirectoryInfo(".").EnumerateDirectories())
                {
                    foreach (var fi in di.EnumerateFiles("*.*", SearchOption.AllDirectories))
                    {
                        if (SelectRestoreFiles(fi))
                        {
                            results.Add(new RestoreResult(fi, msc));
                        }
                    }
                }
            }

            // the code that you want to measure comes here
            watch.Stop();
            var elapsedMs = watch.ElapsedMilliseconds;
            long bytesRestored = 0;
            long filesRestored = 0;
            foreach (var item in results)
            {
                if (string.IsNullOrEmpty(item.Checksum))
                    continue;

                if (item.Success)
                {
                    ++filesRestored;
                    bytesRestored += item.FI.Length;
                }
                Console.WriteLine(item);
            }
            if(filesRestored > 0)
                Console.WriteLine("---------------------------------------------------------------------");
            Console.WriteLine("Restored a total of {0} files using {1} bytes in {2} ms.",
                filesRestored,
                bytesRestored,
                elapsedMs);
        }

        private string CurrentDirectory;

        private bool SelectBackupFiles(FileInfo fi)
        {
            Debug.Assert(fi.FullName.StartsWith(CurrentDirectory, StringComparison.CurrentCultureIgnoreCase));

            string subname = fi.FullName.Substring(CurrentDirectory.Length+1);
            foreach(string excludeFolder in Configuration.ExcludeFolders)
            {
                if (subname.StartsWith(excludeFolder, StringComparison.CurrentCultureIgnoreCase))
                    return false;
            }
            
            // always use files exceeding the cutoff size
            if (fi.Length > Configuration.CutoffSize)
                return true;

            string e = Path.GetExtension(fi.Name).ToLower();
            if (e.StartsWith("."))
                e = e.Substring(1);

            if (Configuration.IncludeExtensions.Contains(e))
                return true;

            return false;
        }

        private void Backup()
        {
            var msc = Configuration.CurrentConfig;
            var watch = System.Diagnostics.Stopwatch.StartNew();
            List<BackupResult> results;

            CurrentDirectory = Directory.GetCurrentDirectory();

            if (msc.SupportsParallelExecution)
            {
                results = new List<BackupResult>(
                     new DirectoryInfo(CurrentDirectory).EnumerateDirectories()
                            .AsParallel()
                            .SelectMany(di => di.EnumerateFiles("*.*", SearchOption.AllDirectories)
                                                .Where(SelectBackupFiles))
                            .AsParallel()
                            .AsOrdered().Select(x => new BackupResult(x, msc))
                            .ToArray());
            }
            else
            {
                results = new List<BackupResult>();
                foreach(var di in new DirectoryInfo(CurrentDirectory).EnumerateDirectories())
                {
                    foreach(var fi in di.EnumerateFiles("*.*", SearchOption.AllDirectories))
                    {
                        if(SelectBackupFiles(fi))
                        {
                            results.Add(new BackupResult(fi, msc));
                        }
                    }
                }
            }

            watch.Stop();
            var elapsedMs = watch.ElapsedMilliseconds;
            long bytesBackedUp = 0;
            long filesBackedUp = 0;
            foreach (BackupResult item in results)
            {
                if (string.IsNullOrEmpty(item.Checksum))
                    continue;

                if (item.Success)
                {
                    ++filesBackedUp;
                    bytesBackedUp += item.FI.Length;
                }
                Console.WriteLine(item);
            }
            if (filesBackedUp > 0)
                Console.WriteLine("---------------------------------------------------------------------");
            Console.WriteLine("Backed up a total of {0} files using {1} bytes in {2} ms.",
                filesBackedUp,
                bytesBackedUp,
                elapsedMs);
        }

        static void Main(string[] args)
        {
            new Program(args).Run();
            Console.ReadLine();
        }
    }
}
