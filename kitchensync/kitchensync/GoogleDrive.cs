﻿using Google.Apis.Auth.OAuth2;
using Google.Apis.Download;
using Google.Apis.Drive.v3;
using Google.Apis.Drive.v3.Data;
using Google.Apis.Services;
using Google.Apis.Upload;
using Google.Apis.Util.Store;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace kitchensync
{
    /*
     * The general concept for BACKUP works like this:
     * 
     * - check which files exist in the google drive cache
     * - check all local files. if a file > CUTOFF size, create a copy on the google drive
     *   and replace the local one with an XML file referencing the google drive
     *  
     * The general concept for RESTORE works like this:
     * 
     * - check which files exist in the google drive cache  
     * - check all local files. if the file is a XML file referencing the google drive,  
     *   replace it with the content from there.
     *   
     * This indicates that there is one starting function:
     * 
     * - build up an internal representation of the files on the google drive
     */

    class GoogleDrive
    {
        // at ~/.credentials/drive-dotnet-quickstart.json
        static string[] Scopes = { DriveService.Scope.Drive};
        static string ApplicationName = "kitchensync";

        public readonly UserCredential Credential;
        public readonly DriveService Service;
        private readonly string RemotePath;
        private readonly string RemotePathFolderId;

        public GoogleDrive(string clientSecretFilename, string remotePath)
        {
            RemotePath = remotePath;
            using (var stream =
                new FileStream(clientSecretFilename, FileMode.Open, FileAccess.Read))
            {
                string credPath = System.Environment.GetFolderPath(
                    System.Environment.SpecialFolder.Personal);
                credPath = Path.Combine(credPath, ".credentials/drive-dotnet-quickstart.json");

                Credential = GoogleWebAuthorizationBroker.AuthorizeAsync(
                    GoogleClientSecrets.Load(stream).Secrets,
                    Scopes,
                    "user",
                    CancellationToken.None,
                    new FileDataStore(credPath, true)).Result;
                Console.WriteLine("Credential file saved to: " + credPath);
            }

            // Create Drive API service.
            Service = new DriveService(new BaseClientService.Initializer()
            {
                HttpClientInitializer = Credential,
                ApplicationName = ApplicationName,
            });

            RemotePathFolderId = GetFolderIdFromPath(RemotePath);

        }


        private string GetFolderIdFromPath(string folderPath)
        {
            string parentId = "root";

            foreach (string subFolderName in folderPath.Split('\\', '/'))
            {
                if (string.IsNullOrEmpty(subFolderName))
                    continue;
                parentId = GetFolderId(parentId, subFolderName);
            }
            return parentId;
        }

        public IList<Google.Apis.Drive.v3.Data.File> ListDirectory(string folderPath)
        {
            string parentId = "root";

            foreach(string subFolderName in folderPath.Split('\\', '/'))
            {
                if (string.IsNullOrEmpty(subFolderName))
                    continue;
                parentId = GetFolderId(parentId, subFolderName);
            }

            // Define parameters of request.
            FilesResource.ListRequest listRequest = Service.Files.List();
            listRequest.PageSize = 512;
            listRequest.Fields = "nextPageToken, files(id, name, mimeType)";
            listRequest.Q = string.Format("'{0}' in parents and trashed=false", parentId);
            var response = listRequest.Execute();
            var fl = response as Google.Apis.Drive.v3.Data.FileList;
            if(fl != null)
            {
                return fl.Files;
            }
            return null;
        }

        private string GetFileIdFromChecksum(string checksum)
        {
            // Define parameters of request.
            FilesResource.ListRequest listRequest = Service.Files.List();
            listRequest.PageSize = 512;
            listRequest.Fields = "nextPageToken, files(id, name, mimeType)";
            listRequest.Q = string.Format("name='{0}' and trashed=false", checksum);
            var response = listRequest.Execute();
            var fl = response as Google.Apis.Drive.v3.Data.FileList;
            if (fl != null)
            {
                if(fl.Files.Count > 0)
                {
                    return fl.Files[0].Id;
                }
            }
            return null;
        }



        public void DownloadFile(string checksum, string localFilePath)
        {
            var fileId = GetFileIdFromChecksum(checksum);
            if (string.IsNullOrEmpty(fileId))
                return;
            var request = Service.Files.Get(fileId);
            //var stream = new System.IO.MemoryStream();
            var stream = new FileStream(localFilePath, FileMode.Create, FileAccess.Write);

            // Add a handler which will be notified on progress changes.
            // It will notify on each chunk download and when the
            // download is completed or failed.
            request.MediaDownloader.ProgressChanged +=
                    (IDownloadProgress progress) =>
                    {
                        switch (progress.Status)
                        {
                            case DownloadStatus.Downloading:
                                {
                                    Console.WriteLine(progress.BytesDownloaded);
                                    break;
                                }
                            case DownloadStatus.Completed:
                                {
                                    Console.WriteLine("Download {0} complete.", localFilePath);
                                    break;
                                }
                            case DownloadStatus.Failed:
                                {
                                    Console.WriteLine("Download {0} failed.", localFilePath);
                                    break;
                                }
                        }
                    };
            request.Download(stream);
        }

        public bool UploadFile(string checksum, string localFilePath)
        {
            var fileId = GetFileIdFromChecksum(checksum);

            // if the file already exists, we don't need to upload it 
            if (!string.IsNullOrEmpty(fileId))
            {
                Console.WriteLine("- {0} is already on Google Drive", localFilePath);
                return false;
            }

            if(string.IsNullOrEmpty(RemotePathFolderId))
            {
                Console.WriteLine("ERROR: unable to find parent folder {0}", RemotePath);
                return false;
            }

            var body = new Google.Apis.Drive.v3.Data.File();
            body.Name = checksum;
            body.MimeType = GetMimeType(localFilePath);
            body.Parents = new List<string>() { RemotePathFolderId };

            // File's content.
            byte[] byteArray = System.IO.File.ReadAllBytes(localFilePath);
            System.IO.MemoryStream stream = new System.IO.MemoryStream(byteArray);
            try
            {
                var request = Service.Files.Create(body, stream, body.MimeType);
                request.Upload();
                Console.WriteLine("{0} uploaded successfully as {1}", localFilePath, checksum);
                return true;
            }
            catch (Exception e)
            {
                Console.WriteLine("Error {0} occurred while uploading {1}", e.Message, localFilePath);
                return false;
            }
        }


        private string GetMimeType(string fileName)
        {
            string mimeType = "application/unknown";
            string ext = System.IO.Path.GetExtension(fileName).ToLower();
            Microsoft.Win32.RegistryKey regKey = Microsoft.Win32.Registry.ClassesRoot.OpenSubKey(ext);
            if (regKey != null && regKey.GetValue("Content Type") != null)
                mimeType = regKey.GetValue("Content Type").ToString();
            return mimeType;
        }

        
        private string GetFolderId(string parentId, string subFolderName)
        {
            // Define parameters of request.
            FilesResource.ListRequest listRequest = Service.Files.List();
            listRequest.PageSize = 5;
            listRequest.Fields = "nextPageToken, files(id, name, mimeType)";
            listRequest.Q = string.Format("'{0}' in parents and name='{1}' and mimeType='application/vnd.google-apps.folder' and trashed=false",
                parentId, subFolderName);

            var response = listRequest.Execute();
            var fl = response as Google.Apis.Drive.v3.Data.FileList;
            if (fl != null)
            {
                foreach(var file in fl.Files)
                {
                    return file.Id;
                }
            }
            return null;
        }
    }
}
