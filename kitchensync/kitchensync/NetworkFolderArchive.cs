﻿// Copyright (c) 2017 NG Branch Technology GmbH
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without modification,
// are permitted provided that the following conditions are met:
//
// Redistributions of source code must retain the above copyright notice, this list
// of conditions and the following disclaimer. Redistributions in binary form must
// reproduce the above copyright notice, this list of conditions and the following
// disclaimer in the documentation and/or other materials provided with the distribution.
// 
// Neither the name regdiff nor the names of its contributors may be used to endorse
// or promote products derived from this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
// ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
// WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
// IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
// INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
// NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
// PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
// WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace kitchensync
{
    public class NetworkFolderArchive : IRemoteArchive
    {
        public readonly string BackupFolderName;

        public NetworkFolderArchive(XmlTextReader xmlReader)
        {
            BackupFolderName = xmlReader.GetAttribute("folder");
        }

        public void Backup(FileInfo fi, string checksum)
        {
            CopyFileExactly(fi, Path.Combine(BackupFolderName, checksum), true);
        }

        public void Restore(FileInfo fi, string checksum)
        {
            FileInfo restoreInfo = new FileInfo(Path.Combine(BackupFolderName, checksum));
            CopyFileExactly(restoreInfo, fi.FullName, false);
        }


        public bool SupportsParallelExecution()
        {
            return true;
        }
        private void CopyFileExactly(FileInfo origin, string copyToPath, bool skipExisting)
        {
            if (!skipExisting || !File.Exists(copyToPath))
            {
                origin.CopyTo(copyToPath, true);

                var destination = new FileInfo(copyToPath);
                destination.CreationTime = origin.CreationTime;
                destination.LastWriteTime = origin.LastWriteTime;
                destination.LastAccessTime = origin.LastAccessTime;
            }
        }
    }
}
