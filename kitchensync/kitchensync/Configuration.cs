﻿// Copyright (c) 2017 NG Branch Technology GmbH
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without modification,
// are permitted provided that the following conditions are met:
//
// Redistributions of source code must retain the above copyright notice, this list
// of conditions and the following disclaimer. Redistributions in binary form must
// reproduce the above copyright notice, this list of conditions and the following
// disclaimer in the documentation and/or other materials provided with the distribution.
// 
// Neither the name regdiff nor the names of its contributors may be used to endorse
// or promote products derived from this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
// ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
// WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
// IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
// INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
// NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
// PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
// WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.IO;

namespace kitchensync
{
    public class Configuration
    {
        public class MachineSpecificConfiguration
        {
            public readonly string Description;
            public readonly string Name;
            public readonly List<IRemoteArchive> RemoteArchives = new List<IRemoteArchive>();

            public MachineSpecificConfiguration(XmlTextReader reader)
            {
                Description = reader.GetAttribute("description");
                Name = reader.GetAttribute("name");
            }

            public bool SupportsParallelExecution
            {
                get
                {
                    foreach(var ra in RemoteArchives)
                    {
                        return ra.SupportsParallelExecution();
                    }
                    return false;
                }
            }
        }

        public readonly Dictionary<string, MachineSpecificConfiguration> Machines = new Dictionary<string, MachineSpecificConfiguration>();
        public string ConfigurationFilename = "kitchensync.xml";

        public HashSet<string> IncludeExtensions { get; private set; }

        public HashSet<string> ExcludeFolders { get; private set; }

        public long CutoffSize { get; private set; }
        public bool HasBeenRead { get; private set; }
        public bool RunBackup { get; private set; }
        public bool RunRestore { get; private set; }

        private delegate bool ParserMethod(string arg);

        private bool pm_Default(string arg)
        {
            if (arg.Equals("/CONFIG"))
            {
                CurrentParserMethod = pm_RecordConfigurationFilename;
                return true;
            }

            if (arg.Equals("/BACKUP", StringComparison.CurrentCultureIgnoreCase))
            {
                RunBackup = true;
                return true;
            }

            if (arg.Equals("/RESTORE", StringComparison.CurrentCultureIgnoreCase))
            {
                RunRestore = true;
                return true;
            }
            Console.WriteLine("ERROR: invalid command line argument {0} passed, aborting", arg);
            HasBeenRead = false;
            return false;
        }


        private bool pm_RecordConfigurationFilename(string arg)
        {
            ConfigurationFilename = arg;
            CurrentParserMethod = pm_Default;
            return true;
        }

        private ParserMethod CurrentParserMethod;

        public Configuration(string[] args)
        {
            bool argumentsAreValid = true;
            CurrentParserMethod = pm_Default;
            foreach (string arg in args)
            {
                if(!CurrentParserMethod(arg))
                {
                    argumentsAreValid = false;
                    break;
                }
            }
            if(argumentsAreValid)
            {
                HasBeenRead = ReadConfigurationFile();
            }
        }

        private bool ReadConfigurationFile()
        {
            try
            {
                MachineSpecificConfiguration msc = null;

                string directoryName = Path.GetDirectoryName(ConfigurationFilename);
                if(!string.IsNullOrEmpty(directoryName))
                {
                    Environment.CurrentDirectory = directoryName;
                }
                ExcludeFolders = new HashSet<string>();
                ExcludeFolders.Add(".git\\");

                using (XmlTextReader xmlReader = new XmlTextReader(Path.GetFileName(ConfigurationFilename)))
                {
                    while (xmlReader.Read())
                    {
                        if (xmlReader.NodeType == XmlNodeType.Element)
                        {
                            if (xmlReader.Name == "kitchensync")
                            {
                                // ok
                            }
                            else if (xmlReader.Name == "settings")
                            {
                                CutoffSize = long.Parse(xmlReader.GetAttribute("cutoff-size"));

                                string include = xmlReader.GetAttribute("include");
                                if (!string.IsNullOrEmpty(include))
                                {
                                    IncludeExtensions = new HashSet<string>(include.Split('|'));
                                }
                            }
                            else if (xmlReader.Name == "exclude")
                            {
                                string folder = xmlReader.GetAttribute("folder");
                                if (!string.IsNullOrEmpty(folder))
                                {
                                    ExcludeFolders.Add(folder + "\\");
                                }
                            }

                            else if (xmlReader.Name == "machine")
                            {
                                msc = new MachineSpecificConfiguration(xmlReader);
                                Machines[msc.Name] = msc;
                            }
                            else if (msc != null)
                            {
                                if (xmlReader.Name == "network")
                                {
                                    msc.RemoteArchives.Add(new NetworkFolderArchive(xmlReader));
                                }
                                else if (xmlReader.Name == "google-drive")
                                {
                                    msc.RemoteArchives.Add(new GoogleDriveArchive(xmlReader));
                                }
                                else
                                {
                                    Console.WriteLine("WARNING: invalid tag {0} in kitchensync.xml for machine {1}, ignored...",
                                        xmlReader.Name,
                                        msc.Name);
                                }
                            }
                            else
                            {
                                Console.WriteLine("WARNING: invalid tag {0} in kitchensync.xml, ignored...",
                                    xmlReader.Name);
                            }
                        }
                        else if (xmlReader.NodeType == XmlNodeType.EndElement)
                        {
                            if (xmlReader.Name == "machine")
                            {
                                msc = null;
                            }
                        }
                    }
                }
                return true;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }
            return false;
        }

        public bool KnowsThisMachine()
        {
            return Machines.ContainsKey(Environment.MachineName) || Machines.ContainsKey("*");
        }

        public MachineSpecificConfiguration CurrentConfig
        {
            get
            {
                if(Machines.ContainsKey(Environment.MachineName))
                {
                    return Machines[Environment.MachineName];
                }
                return Machines["*"];
            }

        }
    }
}
