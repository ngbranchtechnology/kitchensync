# README #

GIT is a great version control tool. However, it has a known limit that it grows very slow if you have archives > 2GB in size. 

Most sourcecode-only projects will have a much smaller size. Plus, most sourcecode can be compressed with very good ratios (because it is effectively just test). So there is no practical problem for a "sourcecode-only" archive.

However, sometimes you want to track binary files as well. Some examples:

- You might want to track setups - including the setup binaries.
- In game development, you might want to track artefacts such as bitmaps or mp3 files

The basic idea of *kitchensync* is this: 

- BEFORE you do a GIT commit, you replace large files by small *references* to the actual files. The GIT archive only contains "links" to the big files, not their actual content. This is called *BACKUP*.

This is typically OK. You don't really want to do a diff between two large blob files, do you? However, at some point you need to get back the actual blobs. So

- AFTER you do a GIT pull (or a GIT fetch & rebase), you replace the small *references* by their actual content again. This is called *RESTORE*.

## Example: Tracking a setup in GIT ##
You will want to use GIT for many files such as

- configuration data
- the actual installation script
- the resulting 100mb installers

an so on. So the workflow is this:

- You create a local directory for your setup and work like normal. 
- Now you want to commit the changes to GIT:
	- Run `kitchensync /BACKUP` to shrink the biggies
	- Run `git commit` to commit your changes
	- Optionally, run `git push` to store the changes on the central server
	- Run `kitchensync /RESTORE` to get back the biggies in the working folder

It is important to understand that this concept is not strictly tied to GIT. It is also not tied to a particular method of archiving files (we will see different archive methods below).

# So, how does it work ? #

Assume you have a folder with your work, in which you want to be able to backup/restore large files. The first step is: You need to provide a `kitchensync.xml` configuration file, somewhat like this:

    <?xml version="1.0" encoding="utf-8"?>
    <kitchensync>

        <settings cutoff-size="1024000"/>

        <!-- this is the information for specific machines -->
        <machine name="WINDOWS-MACHINE-NAME 1" description="Office Workstation #1">
            <network folder="\\\\SERVER\\SHARE\FOLDER\IS\HERE"/>
        </machine>

        <machine name="WINDOWS-MACHINE-NAME 2" description="Office Workstation #2">
            <network folder="\\\\SERVER\\SHARE\FOLDER\IS\HERE"/>
        </machine>

        ...
        
        <machine name="WINDOWS-MACHINE-NAME n" description="Office Workstation #n">
            <network folder="\\\\SERVER\\SHARE\FOLDER\IS\HERE"/>
        </machine>

    </kitchensync>

The 'cutoff-size' parameter specifies the minimum size a file needs to have for it to be auto-archived. For future versions, we will add more fine-grained control (filters etc). 

You define for every workstation running the `kitchensync` where backups should be stored. For example, in a typical setup you could use a network folder (or a google drive sync folder, or a OneDrive sync folder etc.)

In the second step, you simply run the commands

- `kitchensync /BACKUP` and
- `kitchensync /RESTORE`

in your local folder. That's it! 

## Future plans ##

More backup options: 
- Currently, we only support backup to a network folder. I want to directly support OneDrive / GoogleDrive archiving.
- I want to support OwnCloud archiving.
- I want to support SQL Database archiving.

More filters:
- I want to be able to exclude directories (like for build artefacts and intermediates)
- I want to be able to exclude specific files (e.g. really large sourcecode)
- I want to be able to include specific files (e.g. really small binaries)

A UI for those too poor to understand XML